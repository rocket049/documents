# windows上安装vala

[官方文档](https://wiki.gnome.org/Projects/Vala/ValaOnWindows)

### 看上去最简单的`MSYS2`

[MSYS2工具](http://www.msys2.org/)

#### 用下面３个命令安装64位版`vala`

```
pacman -S mingw-w64-x86_64-gcc 
pacman -S mingw-w64-x86_64-pkg-config
pacman -S mingw-w64-x86_64-vala
```
