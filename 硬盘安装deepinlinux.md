#硬盘安装Deepin Linux 15.5

安装体验一下deepin linux 15.5，因为找不到U盘，是从网上学习了一下硬盘安装方法，现已安装成功，贡献一下我的经验：

##1、准备好一个大小为 20G 的 EXT4 分区

用来做deepin linux的主分区，记住该新分区和HOME分区的标记（预备让新装的deepin linux和原来的ubuntu共用HOME分区）。

##2、把下载的ISO文件存放在“/deepin/deepin-15.5-amd64.iso”

为什么不放在HOME目录？

因为我要让新装的deepin linux和原来的ubuntu共用HOME分区（为免干扰，用户名要不同于ubuntu下的用户名）。

##3、编辑“/etc/grub.d/40_custom”

增加一下内容：

		menuentry "Deepin Linux 15.5 ISO" {
		search --file /deepin/deepin-15.5-amd64.iso --set=root
		set isofile="/deepin/deepin-15.5-amd64.iso"
		loopback loop $isofile
		linux (loop)/live/vmlinuz iso-scan/filename=$isofile boot=live components union=overlay locales=zh_CN.UTF-8
		initrd (loop)/live/initrd.lz
		}

##4、跟新 GRUB2 配置文件

		sudo update-grub
		
重启后就会出现启动项“Deepin Linux 15.5 ISO”

##5、安装中的小问题

进入live桌面后，先连接无线路由器，并在文件管理器内卸载预备的新分区和原来的HOME分区，否则安装程序无法显示这两个分区，只会显示SWAP分区。准备好后点击桌面上的安装程序，开始安装（选择分区时不要改变GRUB2的安装位置）。

注：首次启动进入桌面非常慢，要耐心，不要轻易重启了。

因为我设置了SWAP文件，所以不用SWAP分区。

体验：我装了两台笔记本，一个是2017年买的联想小新 i7，一台是2008年的联想 ideapad Y430，各种驱动都没有出现问题，跑新电脑上的体验非常好，只是在老电脑上运行速度确实比 xubuntu 要慢一些，美观的代价吧。特别要称赞的是竟然可以完美支持QQ、微信（完美支持传文件）、迅雷迷你版，希望它越来越好。
