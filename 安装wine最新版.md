# 安装最新版的wine

## 删除原有版本

	sudo apt-get remove wine* winetricks
	sudo apt-get autoremove

## 启用32位支持

	sudo dpkg --add-architecture i386

## 安装软件源  repository：

	wget -nc https://dl.winehq.org/wine-builds/Release.key
	sudo apt-key add Release.key
	sudo apt-add-repository https://dl.winehq.org/wine-builds/ubuntu/
	sudo apt-get update

## 安装 wine

稳定版 Stable branch：

	sudo apt-get install --install-recommends winehq-stable

开发版 Development branch：

	sudo apt-get install --install-recommends winehq-devel
