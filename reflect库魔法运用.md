#golang标准库reflect的魔法运用

##本文要解决什么问题？

有时候我们为了简化代码，需要包装使用一些参数数量可变的函数，例如数据库查询用的`Query`、`Exec`等函数，你的`SQL`语句中有多少个“?”，后面就要对应提供多少参数。要包装这样的函数，使用`reflect`库是唯一的选择。

##实现方法

废话不多说，下面就说说实质性的内容。

###思路

总体上就是依靠`reflect.Value`来实现，一个`reflect.Value`赋值为要执行的函数，另外一个数组`[]reflect.Value`用来存储参数，最后使用`reflect.Value.Call`方法执行函数。

###实例

咱们就拿最简单的`fmt.Println`来做例子，给它做一个包装，并且返回它的全部返回值，代码如下：

	func WrapPrintln(arg ...interface{}) (int,error) {
	    var f = reflect.ValueOf( fmt.Println )
	    var args = make( []reflect.Value, len(arg) )
	    for i:=0;i<len(arg);i++ {
	    if arg[i]==nil{
		args[i] = reflect.Zero(reflect.TypeOf(errors.New("")))
	    }else{
		args[i] = reflect.ValueOf( arg[i] )
	    }
	    }
	    ret := f.Call( args )
	    n := ret[0].Interface().(int)
	    if ret[1].Interface()!=nil{
		return n,ret[1].Interface().(error)
	    } else {
		return n,nil
	    }
	}

至于上面的例子为什么要多加一个判断参数是否是*nil*，是因为*reflect.ValueOf(nil)*会*panic*，不得已而为之。

*end*
