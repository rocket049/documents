#P2P聊天、Web服务器集成

##构架

客户端用安全连接(TLS)登录服务器，客户端之间的数据通过服务器中转。
客户端同时提供文字聊天、发送文件和图片、Web服务功能。
一次只能进行一次文件传输：发送文件和图片、Web请求只能单线程进行。

##格式化报文

1. 报文长度：uint16
2. 命令：uint8 enum
3. 来源：int64 正数：user ID，负数:group ID
4. 目标：int64 同上
5. 信息：二进制信息段，根据命令，可以是Gob编码信息或者原始信息

###命令(开头加`Cmd`)

1. Chat　　　　　 ＃客户端之间的文字信息
2. HttpRequest　　　　　＃http请求　
3. HttpRespContinued  #不完整数据
4. HttpRespFinal　　　　　＃最后一块数据
5. FileHeader     #包括图片
6. FileContinued　　＃不完整数据
7. FileFinal　　　　　＃最后一块数据
8. Login        #登录
9. LogResult    #登录结果 
10. Register     #注册
11. RegResult   #注册结果
12. CancelFile      #取消文件传输或下载

####1.Chat
UTF-8编码原始信息。

####2.HttpRequest
WebKit发送的原始http请求信息，通过本地Proxy转换为上述报文。

####3.HttpRespContinued
Web服务器返回信息，因超过长度限制(65000)被分段的、不完整的信息，通过本地Proxy转换成上述报文。

####4.HttpRespFinal
Web服务器返回信息，通过本地Proxy转换成上述报文。完整的信息，或者最后一个分段信息，通过本地Proxy转换成上述报文。

####5.FileHeader
主动发送的文件头，包含所有文件头信息，但不包含数据。

####6.FileContinued
文件数据，因超过长度限制(65000)被分段的、不完整的信息。

####7.FileFinal
文件数据，完整的或最后一个分段信息。

####8.Login
Gob编码的登录验证信息。

####9.LogResult
Gob编码的登录返回信息。

####10.Register
Gob编码的注册信息。

####11.RegResult
Gob编码的注册返回信息。

####12.CancelFile
取消当前正在进行的文件传输或下载。
