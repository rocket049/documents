#Linux下Chromium浏览器关联thunder链接到wine-thunder的方法

*作者：*fuhz

在linux下chromium浏览器点击thunder链接会显示使用xdg-open打开，之后则显示无法打开。在网上找了些方法都是要求修改xdg-open脚本的，其实完全没有必要，这个问题的根本原因是没有理解Linux桌面文件（XDG）的打开机制。

##1、安装wine 迅雷精简版，请百度

##2、编辑wine-thunder.sh脚本，保存在$HOME/script/位置
wine-thunder.sh:

		#!/bin/bash
		wine  $HOME/.wine/drive_c/Program\ Files\ \(x86\)/Thunder\ Network/MiniThunder/Bin/ThunderMini.exe $1

脚本结束。

然后将脚本设置为可执行：

		chmod a+x wine-thunder.sh

##3、在/usr/share/applications路径下的新建wine-thunder.desktop文件，定义协议打开方法。
wine-thunder.desktop文件：

		[Desktop Entry]
		Name=wine-thunder
		Exec=/home/用户名/script/wine-thinder.sh %U
		Icon=ThunderMini
		Terminal=false
		Type=Application
		MimeType=x-scheme-handler/thunder;
		Categories=Network;P2P;
		Comment=A client for the thunder network


脚本结束。

其中：

Exec=/home/用户名/script/wine-thunder.sh %U 指明了程序的执行文件和可接受的thunder链接地址
MimeType=x-scheme-handler/thunder; 指明了可接受的协议类型thunder

##4、把它注册到系统中去,在控制台输入命令：

		xdg-mime default wine-thunder.desktop x-scheme-handler/thunder

这样当你在Chromium点击thunder链接时就会自动将链接发给当前wine-thunder.sh处理了，也不会另开一个迅雷进程。
