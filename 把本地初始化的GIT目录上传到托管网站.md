#把本地初始化的GIT目录上传到托管网站

##一、远程新建一个空的git项目：

在网站上新建一个GIT项目，不要初始化。
假设地址是：https://gitee.com/rocket049/vd1.git

##二、本地初始化一个git目录后：

执行下面的命令：
```
git init
touch README.md
git add README.md 
git add .gitignore 
git commit -m "first commit"
git remote add origin https://gitee.com/rocket049/vd1.git
git push -u origin master
```
