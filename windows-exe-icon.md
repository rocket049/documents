# 给go生成的windows exe文件添加icon图标
1. 安装rsrc
```
go get github.com/akavel/rsrc
```

2.  创建manifest文件, 命名：main.exe.manifest ：
```
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
<assemblyIdentity
    version="1.0.0.0"
    processorArchitecture="x86"
    name="controls"
    type="win32"
></assemblyIdentity>
<dependency>
    <dependentAssembly>
        <assemblyIdentity
            type="win32"
            name="Microsoft.Windows.Common-Controls"
            version="6.0.0.0"
            processorArchitecture="*"
            publicKeyToken="6595b64144ccf1df"
            language="*"
        ></assemblyIdentity>
    </dependentAssembly>
</dependency>
</assembly>
```
3. 生成syso文件
```
rsrc -manifest main.exe.manifest -ico rc.ico -o main.syso
```
4. 将生成的main.syso文件拷贝到main.go同级目录
5. 编译生成main.exe
```
go build -o main.exe Project/main
```
