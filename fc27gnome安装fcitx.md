# 在 Fedora 26/27 GNOME 3.24/3.26 环境中安装 FCITX 小企鹅输入法（修订）

之前我曾经写过一篇文章介绍在 GNOME 3.x 下安装小企鹅输入法框架，但最近在 Fedora 26/27 环境下发现老方法已经失效了，会导致 GNOME 3.24/3.26 桌面在重启后无法进入，探索了一段时间后终于发现了关键问题所在，总结出一套新的方法，可以顺利安装并使用 FCITX 了，具体如下：

1. 删除系统自带的 ibus 输入法框架，使用命令 sudo dnf remove ibus 即可。然后在系统设置->键盘设置中，把输入法快捷键禁用掉。
2. 安装 fcitx，但注意不要再安装 im-chooser 了，因为 Fedora 26/27 中已经不存在 org.gnome.settings-daemon.plugins.keyboard 这个 dconf 注册表项了，改为 /etc/xdg/autostart/org.gnome.SettingsDaemon.Keyboard.desktop 这个特殊的系统自启动文件，这个文件不能删除，否则系统重启后无法进入桌面并卡死！执行如下命令：
	
		$ sudo dnf install fcitx fcitx-cloudpinyin fcitx-configtool fcitx-gtk2 fcitx-gtk3

3. 通过 gnome-tweak-tool 工具将 fcitx 添加为会话自启动程序，这样在每次登录成功进入 GNOME 桌面后就会自动启动 fcitx。
4. 在 /etc/profile.d 中增加一个配制脚本命名为 fcitx.sh，内容如下：

		export GTK_IM_MODULE=fcitx
		export QT_IM_MODULE=fcitx
		export XMODIFIERS="@im=fcitx"

5. 安装了 FCITX 的 Linux 系统，在每次重启、关机时，都会出现很长时间的等待，这是因为 fcitx 进程没有正常响应 dbus 系统退出信号造成的，为了解决这个问题，可以在系统退出前手动杀掉 fcitx 进程即可。但如果想通过 rc.6 模式或者 systemd 方式进行重启或关机前自动杀 fcitx 其实效果并不好。我们换一种思路，修改重启或关机时 systemd 等待 fcitx 进程的时长，默认是 90 秒，我们可以把它改为最多只等待 10 秒，就会起到快速重启或关机不再长时间等待的效果了。具体是修改 /etc/systemd/system.conf 文件，将其中的 #DefaultTimeoutStopSec=90s 这一行前面的 # 去掉，然后将其赋值改为 10s 保存即可。
6. 关闭 Wayland 显示系统，改为 Xorg 显示模式。因为目前 FCITX 对于 Wayland 的支持还很差，而 Fedora 26/27 GNOME 桌面默认是采用 Wayland 显示系统的，这会导致 FCITX 在 gnome 终端等应用程序中输入中文时的光标跟随出现问题，所以禁用 Wayland。方法是修改 /etc/gdm/custom.conf 文件，将里面 #WaylandEnabled=false 这一行前面的“#”删除，保存后重启即可将显示模式改为 Xorg。
7. 重启系统，登录并进入 GNOME 3.24/3.26 桌面后即可正常使用 FCITX 输入法了，并且不会再出现系统重启或关机时长时间等待了。上述方法对于以后更高版本的 GNOME 应该是同样适用的。

*end*
