# 解决go语言编写的QT5程序无法用fcitx输入中文的问题

我发现用`github.com/therecipe/qt`编写的小程序无法使用`fcitx`输入中文。
网上各种设置XIM变量的fcitx都已经设置了，还是没用。
同时`qt creator`是可以用`fcitx`输入中文的。

然后我用`ldd`查看程序链接的动态库发现编译出来的程序链接了`$GOPATH/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/`目录里的动态库，
而没有使用系统目录中的动态库，于是我检查了目录`$GOPATH/src/github.com/therecipe/env_linux_amd64_512/5.12.0/gcc_64/plugins/platforminputcontexts/`，
发现里面缺少动态库`libfcitxplatforminputcontextplugin.so`，我抱着试一试的态度，把
`/usr/lib/x86_64-linux-gnu/qt5/plugins/platforminputcontexts/libfcitxplatforminputcontextplugin.so`
复制到该目录下，然后打开我的小程序，发现已经可以用`fcitx`输入中文了。
