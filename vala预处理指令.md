# vala 预处理指令
*百度了很久都找不到关于vala程序预处理指令的中文资料，于是我到官网找了英文资料，现在作一下简单介绍。本身就很简单！*

### 预处理指令的作用
当我们需要选择性的编译某一部分代码时，例如某个函数在windows上和linux上需要不同的代码来实现，
就需要在程序中加入预处理指令，编译时使用宏来指定跳过哪些代码、编译哪些代码。

### 逻辑选择指令
一共有4个：

1. `#if` - 如果，后面跟一个宏或宏表达式
2. `#elif` - 否则如果，后面跟一个宏或宏表达式
3. `#else` - 否则
4. `#endif` - 结尾

上面4个指令的意思一目了然，`#if`和`#elif`后面跟上宏作为条件。程序中是这样的：

```
#if COND1
    [ vala code 1 ]
#elif COND2
    [ vala code 2 ]
#else
    [ vala code 3 ]
#endif
```

### 定义宏
`vala`程序内部是不能定义宏的，只能使用`valac`的参数`-D`定义，用法如下：

```
valac -D CONDX program.vala
```

### 宏表达式
`#if`和`#elif`后面的条件表达式不但可以是单独的宏，还可以是几个宏的逻辑表达式，格式如下：

1. “或”表达式： `COND1 [ || COND2 ]`
2. “与”表达式： `COND1 [ && COND2 ]`
3. “非”表达式： `! COND1` （英文资料里`!`后面有空格，经本人验证，有无空格都可以）
4. “等于”表达式： `COND1 == COND2` （一个存在，一个不存在）
5. “不等于”表达式： `COND1 != COND2` （两个都存在）

*一个宏的名字如果定义了，它的值就是`true`，否则就是`false`。利用括号可以组合多个表达式。*

#### 下面是英文资料：\
```
vala-code:
    [ any vala code ] [ pp-condition ] [ any vala code ]
pp-condition:
    #if pp-expression vala-code [ pp-elif ] [ pp-else ] #endif
pp-elif:
    #elif pp-expression vala-code [ pp-elif ]
pp-else:
    #else vala-code
pp-expression:
    pp-or-expression
pp-or-expression:
    pp-and-expression [ || pp-and-expression ]
pp-and-expression:
    pp-binary-expression [ && pp-binary-expression ]
pp-binary-expression:
    pp-equality-expression
    pp-inequality-expression
pp-equality-expression:
    pp-unary-expression [ ==pp-unary-expression ]
pp-inequality-expression:
    pp-unary-expression [ !=pp-unary-expression ]
pp-unary-expression:
    pp-negation-expression
    pp-primary-expression
pp-negation-expression:
    ! pp-unary-expression
pp-primary-expression:
    pp-symbol
    ( pp-expression )
    true
    false
pp-symbol:
    identifier
    
The semantics of the preprocessor are very simple: if the condition is true then the Vala
code surrounded by the preprocessor will be parsed, otherwise it will be ignored. A
symbol evaluates to true if it is defined at compile-time. If a symbol in a preprocessor
directive is not defined, it evaluates to false.
```
