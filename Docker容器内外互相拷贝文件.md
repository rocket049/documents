# Docker容器内外互相拷贝文件

2017年08月02日 10:40:23 陌生的童话城堡 阅读数：621

- 从主机复制到容器 `sudo docker cp host_path containerID:container_path`
- 从容器复制到主机 `sudo docker cp containerID:container_path host_path`
