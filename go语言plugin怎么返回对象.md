# go语言plugin怎么返回对象
`golang`的`plugin`功能用的比较少，官方的示例只有返回函数，而没有返回对象。但是实际应用中`OOP`可以极大提高程序的质量，经过我自己试验，发现`plugin`是可以返回对象的。

###返回类型
我试验的返回对象的类型是空接口，然后用类型断言转换成含有一组约定方法的接口。

###实例
我们首先在主程序中定义一个约定返回接口：

```
type ResObj interface {
    Say(string) int
}
```

插件中的对象定义和返回的接口的方法为：

```
//对象定义
type ObjType struct {
  //内部封装数据
    Name string
}
//Say 方法的实现
func (p *ObjType) Say(s string) int {
    fmt.Printf("%s say: %s\n", p.Name, s)
    return len(s)
}
//返回函数
func New(arg string) interface{}{
    return &ObjType{Name:arg}
}
```

在主程序中调用`plugin`：

```
    p, err := plugin.Open(pluginPathName)
    if err != nil {
        panic(err)
    }
    f, err := p.Lookup("New")
    if err != nil {
        panic(err)
    }
    fn, ok := f.(func(string) interface{})
    if ok == false {
        panic("func type error: New.")
    }
    //New 返回空接口
    obj0 := fn("Fisher")
    //把空接口转换成`ResObj`接口
    obj1, ok := obj0.(ResObj)
    if ok == false {
        panic("Type error: ResObj.")
    }
    //调用接口的方法
    obj1.Say("Hello friend!")
```

*结束*
