#ubuntu18.04 上编译 wxGo 的经验总结

本文所指的 “wxGo” 是特指 “github.com/dontpanic92/wxGo/wx”，本人经历了多次失败后，终于成功编译好了wxGo，总结一下经验。

###1、准备工作

首先从 github 下载源代码 master.zip，大约有 87M，然后解压到 GOPATH/src/github.com/dontpanic92/wxGo。

###2、解决依赖关系：

研究 wxGo/wx/setup_linux_amd64.go，得到以来的软件包列表：

`cgo LDFLAGS: -pthread -lstdc++ -lGL -lGLU -lgthread-2.0 -lX11 -lXxf86vm -lSM -lpangocairo-1.0 -lpango-1.0 -latk-1.0 -lcairo -lnotify -lgdk_pixbuf-2.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0 -lpng -ljpeg -ltiff -lexpat -lz -ldl -lm -lgstreamer-1.0 -lgstvideo-1.0 `

把上面这些依赖的 dev 包都装全。

###3、检查内存是否足够

编译 wxGo 对内存的要求相当变态，在 gnome-shell 环境下，需要的内存总量大约是 9.2 G。

下面是我编译过程中，内存占用最多的时候的内存用量：

内存： 1980 1831 62 0 86 32
交换： 10239 7345 2894

我的电脑比较老，内存只有2G，SWAP本来有4G，远远不够，编译几次失败后，弄明白原因是内存不够后，在编译时临时增加了 6G 交换文件才最终编译成功。

###4、编译命令

go 1.10 编译 wxGo 还要修改参数，否则会报错。

我的办法是在文件 wxGo/utils/build_binary_package.sh 的 79 行后面插入三行：

        export CGO_CXXFLAGS_ALLOW=".*" 
        export CGO_LDFLAGS_ALLOW=".*" 
        export CGO_CFLAGS_ALLOW=".*"

然后运行编译程序：

        ./build_binary_package.sh linux amd64
