# grub2界面重复跳出窗口问题

## 原因

grub 的某个BUG导致 grub.cfg 中的参数：
	terminal_output --append gfxterm
	导致多个终端轮流输出

## 处置方法

修改 */etc/grub.d/00_header* 中的如下代码：
	terminal_output --append ${GRUB_TERMINAL_OUTPUT}

改成：
	terminal_output ${GRUB_TERMINAL_OUTPUT}

